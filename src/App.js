import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter } from 'react-router-dom';

import AppHeader from './components/AppHeader';
import AppRouting from './components/AppRouting';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppHeader />
        <AppRouting />
      </div>
    </BrowserRouter>
  );
}

export default App;
