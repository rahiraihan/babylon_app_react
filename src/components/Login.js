import React, { Component } from 'react';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';
import { withRouter } from "react-router-dom";


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }
  render() {
    return (
      <Container className="App">
        <h4 className="PageHeading">Enter Login Credentials</h4>
        <div id="register">
          <Form className="form">
            <Col>
              {/* <FormGroup row>  
            <Label for="name" sm={2}>Candidate Id</Label>  
            <Col sm={10}>  
              <Input type="text" name="candidateId" onChange={this.handleChange} value={this.state.candidateId} placeholder="Enter Candidate Id" />  
            </Col>  
          </FormGroup>   */}
              <FormGroup row>
                <Label for="address" sm={2}>User Name</Label>
                <Col sm={10}>
                  <Input type="text" name="username" onChange={this.handleChange} defaultValue={this.state.username} placeholder="User Name" />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="Password" sm={2}>Password</Label>
                <Col sm={10}>
                  <Input type="text" name="password" onChange={this.handleChange} defaultValue={this.state.password} placeholder="Password" />
                </Col>
              </FormGroup>
            </Col>
            <Col>
              <FormGroup row>
                <Col sm={5}>
                </Col>
                <Col sm={1}>
                  {/* <input type="submit" className="button"  value="Register"/>  */}
                  <button type="button" onClick={this.addCandidate} className="button">Submit</button>
                </Col>
                {/* <Col sm={1}>  
              <Button color="danger">Cancel</Button>{' '}  
            </Col>   */}
                <Col sm={5}>
                </Col>
              </FormGroup>
            </Col>
          </Form>
        </div>
      </Container>
    );
  }
}
const style = {
  margin: 15,
};
export default withRouter(Login);