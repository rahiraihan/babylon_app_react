import React,{Component} from 'react';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';

const style = {
    margin: 15,
   };
class CreateEmployee extends Component {
constructor(props){
  super(props);
  this.state={
            employeeId:'',
            firstName:'',
            lastName:'',
            birthDate: '',
            age:'',
            sex:'',
            address:'',
            employedDate:''
  }
 }
render() {
    return (
      <Container className="App">  
      <h4 className="PageHeading">Enter Your Information</h4>  
     <div id="register">
      <Form className="form">  
        <Col>  
          {/* <FormGroup row>  
            <Label for="name" sm={2}>Candidate Id</Label>  
            <Col sm={10}>  
              <Input type="text" name="EmployeeId" onChange={this.handleChange} value={this.state.employeeId} placeholder="Enter Candidate Id" />  
            </Col>  
          </FormGroup>   */}
          <FormGroup row>  
            <Label for="address" sm={2}>First Name</Label>  
            <Col sm={10}>  
              <Input type="text" name="firstName" onChange={this.handleChange} value={this.state.firstName} placeholder="First Name" />  
            </Col>  
          </FormGroup>  
          <FormGroup row>  
            <Label for="lastName" sm={2}>lastName</Label>  
            <Col sm={10}>  
              <Input type="text" name="lastName" onChange={this.handleChange} value={this.state.lastName} placeholder="Last Name" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="birthDate" sm={2}>birthDate</Label>  
            <Col sm={10}>  
              <Input type="text" name="birthDate" onChange={this.handleChange} value={this.state.birthDate} placeholder="birthDate" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="age" sm={2}>age</Label>  
            <Col sm={10}>  
              <Input type="text" name="age" onChange={this.handleChange} value={this.state.age} placeholder="age" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="sex" sm={2}>sex</Label>  
            <Col sm={10}>  
              <Input type="text" name="sex" onChange={this.handleChange} value={this.state.sex} placeholder="sex" />  
            </Col>  
          </FormGroup> 

          <FormGroup row>  
            <Label for="address" sm={2}>address</Label>  
            <Col sm={10}>  
              <Input type="text" name="address" onChange={this.handleChange} value={this.state.address} placeholder="address" />  
            </Col>  
          </FormGroup>
          
          <FormGroup row>  
            <Label for="employedDate" sm={2}>employedDate</Label>  
            <Col sm={10}>  
              <Input type="text" name="employedDate" onChange={this.handleChange} value={this.state.employedDate} placeholder="employedDate" />  
            </Col>  
          </FormGroup>
        </Col>  
        <Col>  
          <FormGroup row>  
            <Col sm={5}>  
            </Col>  
            <Col sm={1}> 
            {/* <input type="submit" className="button"  value="Register"/>  */}
            <button type="button" onClick={this.addCandidate} className="button">Submit</button>  
            </Col>  
            {/* <Col sm={1}>  
              <Button color="danger">Cancel</Button>{' '}  
            </Col>   */}
            <Col sm={5}>  
            </Col>  
          </FormGroup>  
        </Col>  
      </Form>  
      </div>
    </Container>  
    );
  }
}

export default CreateEmployee;