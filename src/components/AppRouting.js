import React, { Component } from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Login from "./Login";
import CreateEmployee from "./employee/CreateEmployee";
import CreateLeave from "./leave/CreateLeave";
import CreateLoan from "./loan/CreateLoan";
import LoanList from "./loan/LoanList";
import LeaveList from "./leave/LeaveList";
import EmployeeList from "./employee/EmployeeList";
import Landing from "./Landing";

class AppRouting extends Component {


    render() {
        return (
            <Router>
                    <Switch>
                        <Route path='/' component={Landing} exact />
                        <Route path='/login' component={Login} exact />
                        <Route path="/employee" component={CreateEmployee} exact />
                        <Route path="/leave" component={CreateLeave} exact />
                        <Route path="/loan" component={CreateLoan} exact />
                        <Route path="/loanList" component={LoanList} exact />
                        <Route path="/leaveList" component={LeaveList} exact />
                        <Route path="/employeeList" component={EmployeeList} exact />
                    </Switch>
            </Router>
        );
    }
}

export default AppRouting;