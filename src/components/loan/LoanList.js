import React, {Component} from 'react';
import axios from "axios";
import MUIDataTable from "mui-datatables";
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import IconButton from '@material-ui/core/IconButton';


class LoanList extends Component {
  
    constructor(props) {
        super(props);
        this.state = {
            data: {
                CandidateData: []
            },
            CandidateData: []
        }
        
    };
    componentDidMount() {
        this.getCandidateList()
    };

    getCandidateList() {
        axios.get("https://localhost:44350/api/Candidate/GetCandidateList").then(response => {  
          console.log(response.data);  
            this.setState({  
                CandidateData: response.data  
            });  
        });  
    };

    render() {  
        const columns = [
            "Candidate Id", 
            "Candidate Full Name",
            "Candidate Mobile",
            "Candidate Email",
            "Candidate Age",
            "Candidate BloodGroup",
            "Candidate Address"];
            const options = {
                filterType: 'checkbox',
              };
        // const {results} = this.state.CandidateData;
        return (  
            <div>  
                <IconButton color="secondary" title="Create Class">
                    <ControlPointIcon fontSize="large"/>
                </IconButton> 
                    <MUIDataTable 
                    title={"Loan List"} 
                    data={
                    this.state.CandidateData.map(item => {
                    return [
                    item.candidateId,
                    item.candidateFullName,
                    item.candidateMobile,
                    item.candidateEmail,
                    item.candidateAge,
                    item.candidateBloodGroup,
                    item.candidateAddress
                                        ]
                    })}

                    //  data = {results}
                    columns={columns} 
                    options={options} 
                    />
  
  
            </div>  
        )  
    }  
}  


export default LoanList;