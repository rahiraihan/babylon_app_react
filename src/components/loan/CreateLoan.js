import React,{Component} from 'react';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';

const style = {
    margin: 15,
   };
class CreateLoan extends Component {
constructor(props){
  super(props);
  this.state={

    loanId:'',
    typeOfLoan:'',
    dateOfApplication:'',
    proposedAmount:'',
    noOfInstRecovery: '',
    startDateOfRecovery:'',
    endDateOfRecovery:'',
    noOfDays:'',
    interestRate:'',
    interestAmount:'',
  }
 }
render() {
    return (
      <Container className="App">  
      <h4 className="PageHeading">Enter Your Information</h4>  
     <div id="register">
      <Form className="form">  
        <Col>  
          {/* <FormGroup row>  
            <Label for="name" sm={2}>Candidate Id</Label>  
            <Col sm={10}>  
              <Input type="text" name="candidateId" onChange={this.handleChange} value={this.state.candidateId} placeholder="Enter Candidate Id" />  
            </Col>  
          </FormGroup>   */}
          <FormGroup row>  
            <Label for="typeOfLoan" sm={2}>typeOf Loan</Label>  
            <Col sm={10}>  
              <Input type="text" name="typeOfLoan" onChange={this.handleChange} value={this.state.typeOfLoan} placeholder="typeOfLoan" />  
            </Col>  
          </FormGroup>  
          <FormGroup row>  
            <Label for="dateOfApplication" sm={2}>dateOfApplication</Label>  
            <Col sm={10}>  
              <Input type="text" name="dateOfApplication" onChange={this.handleChange} value={this.state.dateOfApplication} placeholder="dateOfApplication" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="proposedAmount" sm={2}>proposedAmount</Label>  
            <Col sm={10}>  
              <Input type="text" name="proposedAmount" onChange={this.handleChange} value={this.state.proposedAmount} placeholder="proposedAmount" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="noOfInstRecovery" sm={2}>noOfInstRecovery</Label>  
            <Col sm={10}>  
              <Input type="text" name="noOfInstRecovery" onChange={this.handleChange} value={this.state.noOfInstRecovery} placeholder="noOfInstRecovery" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="startDateOfRecovery" sm={2}>startDateOfRecovery</Label>  
            <Col sm={10}>  
              <Input type="text" name="startDateOfRecovery" onChange={this.handleChange} value={this.state.startDateOfRecovery} placeholder="startDateOfRecovery" />  
            </Col>  
          </FormGroup> 

          <FormGroup row>  
            <Label for="endDateOfRecovery" sm={2}>endDateOfRecovery</Label>  
            <Col sm={10}>  
              <Input type="text" name="endDateOfRecovery" onChange={this.handleChange} value={this.state.endDateOfRecovery} placeholder="endDateOfRecovery" />  
            </Col>  
          </FormGroup> 

          <FormGroup row>  
            <Label for="noOfDays" sm={2}>noOfDays</Label>  
            <Col sm={10}>  
              <Input type="text" name="noOfDays" onChange={this.handleChange} value={this.state.noOfDays} placeholder="noOfDays" />  
            </Col>  
          </FormGroup>

           <FormGroup row>  
            <Label for="interestRate" sm={2}>interestRate</Label>  
            <Col sm={10}>  
              <Input type="text" name="interestRate" onChange={this.handleChange} value={this.state.interestRate} placeholder="interestRate" />  
            </Col>  
          </FormGroup>  

          <FormGroup row>  
            <Label for="interestAmount" sm={2}>interestAmount</Label>  
            <Col sm={10}>  
              <Input type="text" name="interestAmount" onChange={this.handleChange} value={this.state.interestAmount} placeholder="interestAmount" />  
            </Col>  
          </FormGroup>
        </Col>  
        <Col>  
          <FormGroup row>  
            <Col sm={5}>  
            </Col>  
            <Col sm={1}> 
            {/* <input type="submit" className="button"  value="Register"/>  */}
            <button type="button" onClick={this.addCandidate} className="button">Submit</button>  
            </Col>  
            {/* <Col sm={1}>  
              <Button color="danger">Cancel</Button>{' '}  
            </Col>   */}
            <Col sm={5}>  
            </Col>  
          </FormGroup>  
        </Col>  
      </Form>  
      </div>
    </Container>  
    );
  }
}

export default CreateLoan;