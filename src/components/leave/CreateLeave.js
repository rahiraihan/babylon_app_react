import React,{Component} from 'react';
import { Container, Col, Form, Row, FormGroup, Label, Input, Button } from 'reactstrap';

const style = {
    margin: 15,
   };
class CreateLeave extends Component {
constructor(props){
  super(props);
  this.state={
    leaveId:'',
    employeeId:'',
    month:'',
    fromDate:'',
    toDate:'',
    noOfDays:'',
    leavePeriod:''
  }
 }
render() {
    return (
      <Container className="App">  
      <h4 className="PageHeading">Enter Your Information</h4>  
     <div id="register">
      <Form className="form">  
        <Col>  
          {/* <FormGroup row>  
            <Label for="name" sm={2}>Candidate Id</Label>  
            <Col sm={10}>  
              <Input type="text" name="candidateId" onChange={this.handleChange} value={this.state.candidateId} placeholder="Enter Candidate Id" />  
            </Col>  
          </FormGroup>   */}
          <FormGroup row>  
            <Label for="EmployeeId" sm={2}>Employee Id</Label>  
            <Col sm={10}>  
              <Input type="text" name="employeeId" onChange={this.handleChange} value={this.state.employeeId} placeholder="Employee Id" />  
            </Col>  
          </FormGroup>  
          <FormGroup row>  
            <Label for="Month" sm={2}>Month</Label>  
            <Col sm={10}>  
              <Input type="text" name="month" onChange={this.handleChange} value={this.state.month} placeholder="Month" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="fromDate" sm={2}>fromDate</Label>  
            <Col sm={10}>  
              <Input type="text" name="fromDate" onChange={this.handleChange} value={this.state.fromDate} placeholder="fromDate" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="toDate" sm={2}>toDate</Label>  
            <Col sm={10}>  
              <Input type="text" name="toDate" onChange={this.handleChange} value={this.state.toDate} placeholder="toDate" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="noOfDays" sm={2}>noOfDays</Label>  
            <Col sm={10}>  
              <Input type="text" name="noOfDays" onChange={this.handleChange} value={this.state.noOfDays} placeholder="noOfDays" />  
            </Col>  
          </FormGroup> 
          <FormGroup row>  
            <Label for="leavePeriod" sm={2}>leavePeriod</Label>  
            <Col sm={10}>  
              <Input type="text" name="leavePeriod" onChange={this.handleChange} value={this.state.leavePeriod} placeholder="leavePeriod" />  
            </Col>  
          </FormGroup>
        </Col>  
        <Col>  
          <FormGroup row>  
            <Col sm={5}>  
            </Col>  
            <Col sm={1}> 
            {/* <input type="submit" className="button"  value="Register"/>  */}
            <button type="button" onClick={this.addCandidate} className="button">Submit</button>  
            </Col>  
            {/* <Col sm={1}>  
              <Button color="danger">Cancel</Button>{' '}  
            </Col>   */}
            <Col sm={5}>  
            </Col>  
          </FormGroup>  
        </Col>  
      </Form>  
      </div>
    </Container>  
    );
  }
}

export default CreateLeave;