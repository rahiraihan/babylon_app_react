import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import {withRouter} from "react-router-dom";
import {BrowserRouter as Router,Switch,Route,Link } from "react-router-dom";
const useStyles = theme => ({
    button: {
        webkitTransitionDuration: '0.4s',
        transitionDuration: '0.4s',
        height: '50px',
        width: '50px',
        paddingRight: '4px 12px',
        fontSize: '10px',
        margin: '10px',
    },

    sp: {
        display: 'flex',
        justifyContent: 'center'
    },

    i: {
        width: '23%',
        height: '150px',
        backgroundColor: '#E3F2FD',
        margin: '3px',
        color: '#fff'

    },

    p: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '56%'
    },
});


class Landing extends Component {

    render() {
        const { classes } = this.props
        return (
            <React.Fragment>
                <div className={classes.sp}>
                    <div classes={classes.p} >
                        <div classes={classes.i} >  

                            {/* <button classes={classes.button}>
                                <h5>
                                <Link to="/login">Home</Link>
                                </h5>
                            </button> */}

                            <button classes={classes.button} >
                                <h5>
                                <Link to="/employeeList">Employee</Link>
                                </h5>
                            </button>

                            <button classes={classes.button}>
                                <h5>
                                <Link to="/loanList">Loan</Link>
                                </h5>
                            </button>

                            <button classes={classes.button}>
                                <h5>
                                <Link to="/leaveList">Leave</Link>
                                </h5>
                            </button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default withStyles(useStyles)(withRouter(Landing));