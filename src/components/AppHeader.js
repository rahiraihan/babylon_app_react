import React,{Component} from 'react';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import { Box,Button } from "@material-ui/core";
import {BrowserRouter as Router,Switch,Route,Link } from "react-router-dom";





class AppHeader extends Component {

render() {
   return (
   <React.Fragment>
   <Box display="flex" bgcolor="#58D68D" p={2} alignItems="center">
      <Link to="/">Babylon App</Link>
      <br/>
      <br/>
      <Link to="/login">Login</Link>
      <Box flexGrow={1} textAlign="right">
        <IconButton>
          <MenuIcon />
        </IconButton>
      </Box>
    </Box>
</React.Fragment>
    );
}
}

export default (AppHeader);
